$(document).ready(function(){

	
	/*     loader    */

	$(window).load(function() {

		$(".loader_inner").fadeOut();
		$(".loader").delay(400).fadeOut("slow");



	}); // load


	// fixed-header

	$(window).scroll(function(){
	  var sticky = $('.header-wrap'),
	      scroll = $(window).scrollTop();

	  if (scroll >= 60) sticky.addClass('fixed-head');
	  else sticky.removeClass('fixed-head');
	});

	


	// menu button


	$(".toggle-button").click(function(){

		$(".menu").toggleClass("active-menu");
	});

	$(".toggle-button").click(function() {

	  $(".sandwich").toggleClass("active");

	});

	$(".menu li").click(function(){

		$(".menu").removeClass("active-menu");
		$(".sandwich").removeClass("active");
	});


	/*  menu links  */


	$('.to-princip').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-princip').offset().top }, 1000);
	}); 

	$('.to-why').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-why').offset().top }, 1000);
	});

	$('.to-strateg').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-strateg').offset().top }, 1000);
	});

	$('.to-analitic').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('.s-anlytic').offset().top }, 1000);
	});

	$('.to-contact').click(function(e){
	e.preventDefault();
	$('body, html').animate({ scrollTop: $('footer').offset().top }, 1000);
	});










	/*    slick slider    */

	$(".analytic__slider").slick({

	//normal options...
	infinite: true,
	slidesToShow: 4,
	slidesToScroll: 1,
	arrows: true,
	dots: false,

	  // the magic
	 responsive: [
	 	{
		breakpoint: 1024,
			settings: {
			slidesToShow: 3

			}
	  	},
	 	{
		breakpoint: 850,
			settings: {
			slidesToShow: 2,
			}
	  	},  	 	
	 	{
		breakpoint: 580,
			settings: {
			slidesToShow: 1,
			}
	  	}, 

		]

	});//slick


/*   about-company tabs    */

$(".tab-content_item").not(":first").hide();
$(".tab-content_item-right").not(":first").hide();
$(".tab-item").click(function() {
	$(".tab-item").removeClass("active-tab").eq($(this).index()).addClass("active-tab");
	$(".tab-content_item").hide().eq($(this).index()).fadeIn()
	$(".tab-content_item-right").hide().eq($(this).index()).fadeIn()
});


	/* magnific popup*/

$('.open-popup-link').magnificPopup({

    gallery: {
      enabled: true
    },
    type: 'inline', // this is a default type
    midClick: true,
});



$('.image-popup-vertical-fit').magnificPopup({
	type: 'image',
	closeOnContentClick: true,
	mainClass: 'mfp-img-mobile',
	image: {
		verticalFit: true
	}
	
});



}); // ready()




	/*   google map   */


      function initMap() {
        var uluru = {lat: 52.273211, lng: 4.962562};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          icon: '../images/map_icon.png'

        });

        var styles = [
{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-100"},{"lightness":"30"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"gamma":"0.00"},{"lightness":"74"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"lightness":"3"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}
			];

			map.setOptions({styles: styles});
      }